package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> arraySum = new ArrayList<Integer>(); 
    
        for (int i = 0; i < grid.size(); i++) {
            int colSum = 0; 
            int rowSum = 0;

            for (int j = 0; j < grid.get(0).size(); j++) {
                colSum += grid.get(j).get(i);
                rowSum += grid.get(i).get(j);
            }
            arraySum.add(colSum);
            arraySum.add(rowSum);
        }
        Collections.sort(arraySum);
            if (arraySum.get(0) == arraySum.get(arraySum.size() -1)) {
                return true;
            }
            else {
                return false;
            }
    }
    }
