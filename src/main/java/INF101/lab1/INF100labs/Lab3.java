 package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(3);
        System.out.println(crossSum(12));

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int x = 0; n >= x; x++) { //samme som for x in range(n ). Sier at x er først 0, deretter forsette så lenge x er mindre eller samme som n og til slutt at den skal øke med 1 om hver gang 
            if (x > 0 && x % 7 == 0)
                System.out.println(x);
        }                 
             
    }

    public static void multiplicationTable(int n) {
        for (int x = 1; x <= n; x++) {
            System.out.print(x + ": ");
            for (int y = 1; y <= n; y++) {
                System.out.print(y * x + " ");
            }
            System.out.println();        
        }
    
    }
 
    public static int crossSum(int num) {
        int sum = 0;
        while (num > 0) {
            sum += num % 10;
            num = num / 10;
        }
        return sum;
        
    }

}



    
