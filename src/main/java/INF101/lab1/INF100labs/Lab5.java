package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> temp = new ArrayList <>();
        for (Integer num : list) {
            if (!num.equals(3)) temp.add(num);
        }
        return temp;

    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> temp1 = new ArrayList <>();
        for (Integer i : list) {
            if (!temp1.contains(i)){
                temp1.add(i);
            }
        }
        return temp1;

    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        int lenA = a.size();
        for (int i = 0; i < lenA; i++) {
            a.set(i, a.get(i) + b.get(i));
        }
        System.out.println(a);
        System.out.println(b);
    }
}

            
        
